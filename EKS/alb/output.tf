output "alb_id" {
  description = "ID of the generated ALB"
  value = "${aws_lb.alb.id}"
}

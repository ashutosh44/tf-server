resource "aws_efs_file_system" "main" {
  tags {
    Name = "${var.name}"
  }
}

resource "aws_efs_mount_target" "main" {
  count = 2

  file_system_id = "${aws_efs_file_system.main.id}"
  security_groups = ["${element(var.worker-node-sg,0)}"]
  subnet_id      = "${element(var.subnets,count.index)}"
 
  
}



variable "name" {
  default = ""
}


variable "subnets" {
  description = "(Required) A comma separated list of subnet ids where mount targets will be."
  type        = "list"
}


# variable "vpc_id" {
#   default = ""
# }

variable "worker-node-sg" {
  type = "list"
  default = []
}

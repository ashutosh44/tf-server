#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EC2 Security Group to allow networking traffic
#  * Data source to fetch latest EKS worker AMI
#  * AutoScaling Launch Configuration to configure worker instances
#

#IAM Role
resource "aws_iam_role" "worker-node-role" {
  name = "${var.env}_worker_nodes_role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "eks-tagging" {
  name        = "${var.env}_resource_tagging_for_eks"
  path        = "/"
  description = "resource_tagging_for_eks"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "tag:GetResources",
                "tag:UntagResources",
                "tag:GetTagValues",
                "tag:GetTagKeys",
                "tag:TagResources"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "alb_ingress_controller-iam_policy" {
  name        = "${var.env}_alb_ingress_controller-iam_policy"
  path        = "/"
  description = "resource_tagging_for_eks"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "iam:CreateServiceLinkedRole",
            "Resource": "*",
            "Condition": {
                "StringLike": {
                    "iam:AWSServiceName": "elasticfilesystem.amazonaws.com"
                }
            }
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "elasticloadbalancing:ModifyListener",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:DescribeInstances",
                "elasticloadbalancing:RegisterTargets",
                "iam:ListServerCertificates",
                "elasticloadbalancing:SetIpAddressType",
                "elasticloadbalancing:DeleteLoadBalancer",
                "elasticloadbalancing:SetWebAcl",
                "elasticloadbalancing:SetWebACL",
                "ec2:DescribeInternetGateways",
                "elasticloadbalancing:DescribeLoadBalancers",
                "waf-regional:GetWebACLForResource",
                "acm:GetCertificate",
                "waf-regional:GetWebACL",
                "elasticloadbalancing:CreateRule",
                "ec2:DescribeAccountAttributes",
                "elasticloadbalancing:AddListenerCertificates",
                "elasticloadbalancing:ModifyTargetGroupAttributes",
                "waf:GetWebACL",
                "iam:GetServerCertificate",
                "ec2:CreateTags",
                "elasticloadbalancing:CreateTargetGroup",
                "ec2:ModifyNetworkInterfaceAttribute",
                "ec2:DeleteNetworkInterface",
                "elasticloadbalancing:DeregisterTargets",
                "elasticloadbalancing:DescribeLoadBalancerAttributes",
                "ec2:RevokeSecurityGroupIngress",
                "ec2:CreateNetworkInterface",
                "elasticloadbalancing:DescribeTargetGroupAttributes",
                "acm:DescribeCertificate",
                "elasticloadbalancing:ModifyRule",
                "elasticloadbalancing:AddTags",
                "elasticloadbalancing:DescribeRules",
                "ec2:DescribeSubnets",
                "eks:*",
                "elasticloadbalancing:ModifyLoadBalancerAttributes",
                "waf-regional:AssociateWebACL",
                "tag:GetResources",
                "ec2:DescribeAddresses",
                "ec2:DeleteTags",
                "elasticloadbalancing:RemoveListenerCertificates",
                "ec2:DescribeVpcAttribute",
                "tag:TagResources",
                "elasticloadbalancing:RemoveTags",
                "elasticloadbalancing:CreateListener",
                "elasticloadbalancing:DescribeListeners",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DescribeAvailabilityZones",
                "ec2:DescribeNetworkInterfaceAttribute",
                "ec2:CreateSecurityGroup",
                "kms:DescribeKey",
                "acm:ListCertificates",
                "elasticloadbalancing:DescribeListenerCertificates",
                "ec2:ModifyInstanceAttribute",
                "elasticloadbalancing:DeleteRule",
                "ec2:DescribeInstanceStatus",
                "elasticloadbalancing:DescribeSSLPolicies",
                "elasticfilesystem:*",
                "elasticloadbalancing:CreateLoadBalancer",
                "waf-regional:DisassociateWebACL",
                "elasticloadbalancing:DescribeTags",
                "ec2:DescribeTags",
                "elasticloadbalancing:SetSubnets",
                "elasticloadbalancing:DeleteTargetGroup",
                "ec2:DescribeSecurityGroups",
                "iam:CreateServiceLinkedRole",
                "ec2:DescribeVpcs",
                "ec2:DeleteSecurityGroup",
                "kms:ListAliases",
                "elasticloadbalancing:DescribeTargetHealth",
                "elasticloadbalancing:SetSecurityGroups",
                "elasticloadbalancing:DescribeTargetGroups",
                "elasticloadbalancing:ModifyTargetGroup",
                "elasticloadbalancing:DeleteListener"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "worker-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.worker-node-role.name}"
}

resource "aws_iam_role_policy_attachment" "worker-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = "${aws_iam_role.worker-node-role.name}"
}

resource "aws_iam_role_policy_attachment" "worker-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.worker-node-role.name}"
}

resource "aws_iam_role_policy_attachment" "worker-node-AmazonEC2FullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
  role       = "${aws_iam_role.worker-node-role.name}"
}

resource "aws_iam_role_policy_attachment" "worker-node-resource_tagging_for_eks" {
  policy_arn = "${aws_iam_policy.eks-tagging.arn}"
  role       = "${aws_iam_role.worker-node-role.name}"
}

resource "aws_iam_instance_profile" "worker-node" {
  name = "${var.env}_eks-worker_node"
  role = "${aws_iam_role.worker-node-role.name}"
}

resource "aws_iam_role_policy_attachment" "worker-node-ALBIngressControllerIAMPolicy" {
  policy_arn ="${aws_iam_policy.alb_ingress_controller-iam_policy.arn}"
  role       = "${aws_iam_role.worker-node-role.name}"
}
resource "aws_iam_role_policy_attachment" "SecretsManagerReadWrite" {
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite" #alreadey craeted policy
  role       = "${aws_iam_role.worker-node-role.name}"
}
resource "aws_iam_role_policy_attachment" "AmazonS3FullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess" #alreadey craeted policy
  role       = "${aws_iam_role.worker-node-role.name}"
}
resource "aws_iam_role_policy_attachment" "AmazonSSMReadOnlyAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess" #alreadey craeted policy
  role       = "${aws_iam_role.worker-node-role.name}"
}

resource "aws_iam_role_policy_attachment" "AWSCertificateManagerFullAccess" {
  policy_arn = "arn:aws:iam::aws:policy/AWSCertificateManagerFullAccess" #alreadey craeted policy
  role       = "${aws_iam_role.worker-node-role.name}"
}
locals {
  worker-node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.eks-cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.eks-cluster.certificate_authority.0.data}' '${var.cluster_name}'
#!/bin/bash
sudo adduser eksuser
sudo mkdir -p /home/eksuser/.ssh/
sudo touch /home/eksuser/.ssh/authorized_keys
cp /home/ec2-user/.ssh/authorized_keys /home/eksuser/.ssh/authorized_keys
sudo chown -R eksuser:eksuser /home/eksuser/.ssh/
sudo chmod 700 /home/eksuser/.ssh/
sudo chmod 600 /home/eksuser/.ssh/authorized_keys
sudo echo 'eksuser ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/90-cloud-init-users
USERDATA
}


# data "aws_ami" "eks-worker" {
#   filter {
#     name   = "name"
#     values = ["amazon-eks-node-${aws_eks_cluster.eks-cluster.version}-v*"]
#   }

#   most_recent = true
#   owners      = ["364773966936"] # Amazon EKS AMI Account ID
# }

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We utilize a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
#"ami-0619d38218e46ef86" "${data.aws_ami.eks-worker.id}"

##########Launch Configuration
resource "aws_launch_configuration" "worker" {
  iam_instance_profile     = "${aws_iam_instance_profile.worker-node.name}"
  image_id                 = "ami-04e247c4613de71fa"
  key_name                 = "${var.instance_key}"
  instance_type            = "${var.instance_type}"
  name_prefix              = "${var.env}_worker-node"
  security_groups          = ["${var.security_groups}"]
  user_data_base64         = "${base64encode(local.worker-node-userdata)}"

  lifecycle {
    create_before_destroy  = true
  }
}


######Autoscalling Group################
resource "aws_autoscaling_group" "worker" {
  launch_configuration     = "${aws_launch_configuration.worker.id}"
  desired_capacity         = "${var.desired_capacity}"
  max_size                 = "${var.max_size}"
  min_size                 = "${var.min_size}"
  name                     = "${var.env}_worker_nodes"
  vpc_zone_identifier      = ["${var.worker_subnet}"]

  tag {
    key                    = "Name"
    value                  = "${var.env}_worker-nodes"
    propagate_at_launch    = true
  }

  tag {
    key                    = "kubernetes.io/cluster/${var.cluster_name}"
    value                  = "owned"
    propagate_at_launch    = true
  }
}
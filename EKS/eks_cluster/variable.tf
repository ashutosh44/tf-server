

variable "env" {
  description = "env to deploy into, should typically ab_dev/ab_qa/ab_prod"
  default = "ab_dev"
}
# Variables Configuration
variable "cluster_name" {
  description = "EKS cluster name"
}
variable "subnet_ids" {
  type        = "list"
  description = "List of all subnet in cluster"
}



variable "security_group_ids" {
  description = "EKS cluster SG"
  
}

variable "k8s_version" {
  description = "K8s version"
  
}


#############Launch configuration variables########

variable "instance_type" {
  description = "EKS_worker instance type"
  default = ""
}

variable "security_groups" {
  description = "EKS worker SG"
  type = "list"
}


variable "instance_key" {
  description = "EKS worker instance key"
  
}

###### Autoscaling variables#########
variable "worker_subnet" {
  description = "EKS worker subnet ids"
  type = "list"
}

#variable "cluster_name" {
#  description = "EKS cluster name"
#}
# variable "launch_configuration" {
#   description = "launch configuration id"
  
# }

variable "desired_capacity" {
  description = "The number of EC2 instances that should be running in the group"
}

variable "max_size" {
  description = "The maximum size of the auto scale group"
}

variable "min_size" {
  description = "The minimum size of the auto scale group"
}

resource "aws_eip" "ip" {
  instance = "${var.instance_id}"
  vpc      = true

  tags = {
    Name = "${var.elastic_ip_tag_value}"
  }
}

################# Region of Infra #################
provider "aws" {
    version                 = "~> 2.0"
    shared_credentials_file = "${var.key_credentials_path}"
    region                  = "${var.infra_region}"
}

########3 Creating VPC
module "vpc" {
    source               = "../vpc"
    cidr_block           = "${var.vpc_cidr_block}"
    instance_tenancy     = "${var.instanceTenancy}"
    enable_dns_support   = "${var.dnsSupport}"
    enable_dns_hostnames = "${var.dnsHostNames}"
    vpc_Name_tag         = "${var.env}_${var.vpc_tag_name}"
}

#Creating Internet Gateway
module "internat_gateway" {
    source       = "../internat_gateway"
    vpc_id       = "${module.vpc.id}"
    ig_tag_Name  = "${var.env}_${var.igw_name}"
}


#########Creating First Public Subnet ##############
module "jump_server_pub_subnet_az_a" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_pub_subnet_az_a}"
  availability_zone        = "${var.availability_zone_a}"
  subnet_name              = "${var.env}_${var.pub_subnet_name_az_a}"
  cluster_name             = "${var.env}_${var.cluster_name}"
  key                      = "${var.pub_subnet_key_value}"

}
############Creating Second Public Subnet############
module "jump_server_pub_subnet_az_b" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_pub_subnet_az_b}"
  availability_zone        = "${var.availability_zone_b}"
  subnet_name              = "${var.env}_${var.pub_subnet_name_az_b}"
  cluster_name             = "${var.env}_${var.cluster_name}"
  key                      = "${var.pub_subnet_key_value}"
}
###########Creating first Private Subnet#############
module "eks_pvt_subnet_az_a" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_eks_pvt_subnet_az_a}"
  availability_zone        = "${var.availability_zone_a}"
  subnet_name              = "${var.env}_${var.eks_pvt_subnet_name_az_a}"
  cluster_name             = "${var.env}_${var.cluster_name}"
  key                      = "${var.pvt_subnet_key_value}"
}

###########Creating first Jenkins Private Subnet#############
module "jenkins_pvt_subnet_az_a" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_jenkins_pvt_subnet_az_a}"
  availability_zone        = "${var.availability_zone_a}"
  subnet_name              = "${var.env}_${var.jenkins_pvt_subnet_name_az_a}"
  cluster_name             = "${var.env}_${var.cluster_name}"
  key                      = "${var.pvt_subnet_key_value}"
}
##########Creating second Private Subnet#############
module "eks_pvt_subnet_az_b" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_eks_pvt_subnet_az_b}"
  availability_zone        = "${var.availability_zone_b}"
  subnet_name              = "${var.env}_${var.eks_pvt_subnet_name_az_b}"
  cluster_name             = "${var.env}_${var.cluster_name}"
  key                      = "${var.pvt_subnet_key_value}"
}

######## Creating first Private Subnet for DB #############
module "db_pvt_subnet_az_a" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_db_pvt_subnet_az_a}"
  availability_zone        = "${var.availability_zone_a}"
  subnet_name              = "${var.env}_${var.db_pvt_subnet_name_az_a}"
  cluster_name             = "${var.env}_${var.cluster_name}"
  key                      = "${var.pvt_subnet_key_value}"
}
######### Creating second Private Subnet for DB #############
module "db_pvt_subnet_az_b" {
  source                   = "../subnets"
  vpc_id                   = "${module.vpc.id}"
  cidr_subnet              = "${var.cidr_db_pvt_subnet_az_b}"
  availability_zone        = "${var.availability_zone_b}"
  subnet_name              = "${var.env}_${var.db_pvt_subnet_name_az_b}"
  cluster_name             = "${var.env}_${var.cluster_name}"
  key                      = "${var.pvt_subnet_key_value}"
}

########### Creating Nat Gateway ############
module "nat_gateway" {
  source                  = "../nat_gateway"
  subnet_id               = "${module.jump_server_pub_subnet_az_a.subnet_id}"
  natgateway_name         = "${var.env}_${var.natgateway_name}"
}

#####Creating route table for Public Subnets#########
module "pub_route_table" {
  source                  = "../route_table"
  vpc_id                  = "${module.vpc.id}"
  cidr_block_routetable   = "${var.cidr_block_pub_rt}"
  gateway_id              = "${module.internat_gateway.igw_id}"
  tag_Name                = "${var.env}_${var.rt_pub_tag_name}"
}

###########3Associate Public Route Table with jump server public subnets
module "routetable_associations_to_pub_subnet_az_a" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.jump_server_pub_subnet_az_a.subnet_id}"
  routetable_id           = "${module.pub_route_table.routetable_id}"
}
module "routetable_associations_to_pub_subnet_az_b" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.jump_server_pub_subnet_az_b.subnet_id}"
  routetable_id           = "${module.pub_route_table.routetable_id}"
}
#######Creating Private Route Table for Jenkins,EKS & DB Private subnets
module "pvt_route_table" {
  source                  = "../route_table"
  vpc_id                  = "${module.vpc.id}"
  cidr_block_routetable   = "${var.cidr_block_pvt_rt}"
  gateway_id              = "${module.nat_gateway.nat_gateway_id}"
  tag_Name                = "${var.env}_${var.rt_pvt_tag_name}"
}
#########Associate Route Table with EKS private subnets
module "routetable_associations_to_eks_pvt_subnet_az_a" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.eks_pvt_subnet_az_a.subnet_id}"
  routetable_id           = "${module.pvt_route_table.routetable_id}"
}
module "routetable_associations_to_eks_pvt_subnet_az_b" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.eks_pvt_subnet_az_b.subnet_id}"
  routetable_id           = "${module.pvt_route_table.routetable_id}"
}
############## Associate Route Table with DB private subnets
module "routetable_associations_to_db_pvt_subnet_az_a" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.db_pvt_subnet_az_a.subnet_id}"
  routetable_id           = "${module.pvt_route_table.routetable_id}"
}
module "routetable_associations_to_db_pvt_subnet_az_b" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.db_pvt_subnet_az_b.subnet_id}"
  routetable_id           = "${module.pvt_route_table.routetable_id}"
}

############## Associate Route Table with Jenkins private subnets
module "routetable_associations_to_jenkins_pvt_subnet_az_a" {
  source                  = "../associations_routetable"
  subnet_id               = "${module.jenkins_pvt_subnet_az_a.subnet_id}"
  routetable_id           = "${module.pvt_route_table.routetable_id}"
}
   
######## Creating Security group for intstances
module "jump_server_security_group" {
  source                  = "../jump_server_sg"
  security_group_name     = "${var.env}_${var.jump_server_security_group_name}"
  vpc_id                  = "${module.vpc.id}"
  tcp_ports               = "${var.jump_server_ports}"
  cidrs                   = ["${var.cidrs}"]
  tag_name_value          = "${var.env}_${var.jump_server_security_group_tag}"
}

 module "eks_security_group" {
  source                           = "../eks_cluster_sg"
  from_ports                       = "${var.from_ports}"
  to_ports                         = "${var.to_ports}"
  source_security_group_id         = ["${module.jump_server_security_group.pub_sg_id}"]
  security_group_name              = "${var.env}_${var.eks_security_group_name}"
  vpc_id                           = "${module.vpc.id}"
  tag_name_value                   = "${var.env}_${var.eks_security_group_tag}"
}

############## ALB Security Group
module "alb_security_group" {
  source                           = "../jump_server_sg"
  tcp_ports                        = "${var.jenkins_server_ports}"
  cidrs                            = ["${var.jenkins_cidrs}"]
  security_group_name              = "${var.env}_${var.alb_security_group_name}"
  vpc_id                           = "${module.vpc.id}"
  tag_name_value                   = "${var.env}_${var.alb_security_group_tag}"
}

############## Jenkins Security Group
module "jenkins_security_group" {
  source                           = "../security_group"
  from_ports                       = "${var.from_ports_jenkins}"
  to_ports                         = "${var.to_ports_jenkins}"
  source_security_group_id         = ["${module.alb_security_group.pub_sg_id}"]
  security_group_name              = "${var.env}_${var.jenkins_security_group_name}"
  vpc_id                           = "${module.vpc.id}"
  tag_name_value                   = "${var.env}_${var.jenkins_security_group_tag}"
}
############## Redis Security Group
module "redis_security_group" {
  source                           = "../security_group"
  from_ports                       = "${var.from_ports_redis}"
  to_ports                         = "${var.to_ports_redis}"
  source_security_group_id         = ["${module.eks_security_group.pvt_sg_id}"]
  security_group_name              = "${var.env}_${var.redis_security_group_name}"
  vpc_id                           = "${module.vpc.id}"
  tag_name_value                   = "${var.env}_${var.redis_security_group_tag}"
}

############## Jump server
module "jump_server" {
  source                  = "../ec2_server"
  instance_type           = "${var.instance_type}"
  instance_ami            = "${var.instance_ami}"
  instance_key            = "${var.key}"
  subnet_id               = "${module.jump_server_pub_subnet_az_a.subnet_id}"
  security_group          = "${module.jump_server_security_group.pub_sg_id}"
  volume_type             = "${var.volume_type}"
  volume_size             = "${var.volume_size}"
  delete_on_termination   = "${var.delete_on_termination}"
  server_name             = "${var.env}_${var.server_name}"
}

########### Craeting elastic IP 
module "elastic_ip" {
  source                  = "../elastic_ip"
  instance_id             = "${module.jump_server.ec2_id}"
  elastic_ip_tag_value    = "${var.env}_${var.elastic_ip_tag_value}"
}
############## Jenkins server
module "jenkins_server" {
  source                  = "../ec2_server"
  instance_type           = "${var.jenkins_instance_type}"
  instance_ami            = "${var.jenkins_ami}"
  instance_key            = "${var.key}"
  subnet_id               = "${module.jenkins_pvt_subnet_az_a.subnet_id}"
  security_group          = "${module.jenkins_security_group.sg_id}"
  volume_type             = "${var.jenkins_volume_type}"
  volume_size             = "${var.jenkins_volume_size}"
  delete_on_termination   = "${var.delete_on_termination}"
  server_name             = "${var.env}_${var.jenkins_server_name}"
}

########### Craeting EKS cluster 
module "eks" {
  source                        = "../eks_cluster"
  k8s_version                   = "${var.k8s_version}"
  cluster_name                  = "${var.cluster_name}"
  security_group_ids            = "${module.eks_security_group.pvt_sg_id}"
  subnet_ids                    = ["${module.eks_pvt_subnet_az_a.subnet_id}","${module.eks_pvt_subnet_az_b.subnet_id}"]
  instance_key                  = "${var.key}"
  instance_type                 = "${var.instance_type_worker}"
  security_groups               = ["${module.eks_security_group.pvt_sg_id}"]
  worker_subnet                 = ["${module.eks_pvt_subnet_az_a.subnet_id}","${module.eks_pvt_subnet_az_b.subnet_id}"]
  desired_capacity              = "${var.desired_capacity}"
  max_size                      = "${var.max_size}"
  min_size                      = "${var.min_size}"
  cluster_name                  = "${var.env}_${var.cluster_name}"
  env                           = "${var.env}"
}


######### Creating EFS 
# module "efs" {
#   source                       = "../efs"
#   name                         = "AB-EKS-NFS"
#   worker-node-sg               = ["${module.eks_security_group.pvt_sg_id}"]
#   subnets                      = ["${module.efs_pvt_subnet_az_a.subnet_id}", "${module.efs_pvt_subnet_az_b.subnet_id}"]
# }

####### ALB for jenkins Server########
module "alb" {
  source                        = "../alb"
  vpc_id                        = "${module.vpc.id}"
  alb_subnets                   = ["${module.jump_server_pub_subnet_az_a.subnet_id}","${module.jump_server_pub_subnet_az_b.subnet_id}"]
  security_group_id             = "${module.alb_security_group.pub_sg_id}"
  jenkins_ec2_id                = "${module.jenkins_server.ec2_id}"
}


########### elasticache cluster#############
module "redis_cluster" {
  source                                = "../redis"
  node_groups                           = "${var.redis_node_groups}"
  redis_node_type                       = "${var.redis_node_type}"
  redis_port                            = "${var.redis_port}"
  redis_parameter_group_name            = "${var.redis_parameter_group_name}"
  redis_maintenance_window              = "${var.redis_maintenance_window}"
  redis_snapshot_window                 = "${var.redis_snapshot_window}"
  redis_snapshot_retention_limit        = "${var.redis_snapshot_retention_limit}"
  subnet_ids                            = ["${module.db_pvt_subnet_az_a.subnet_id}", "${module.db_pvt_subnet_az_b.subnet_id}"]
  namespace                             = "${var.namespace}"
  cluster_id                            = "${var.cluster_id}"
  security_group_ids                    = ["${module.redis_security_group.sg_id}"]

}

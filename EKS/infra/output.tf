# CIDR blocks
output "kubeconfig" {
  value = "${module.eks.kubeconfig}"
}

output "config_map_aws_auth" {
  value = "${module.eks.config_map_aws_auth}"
}

output "jump_server_ip" {
  value = "${module.elastic_ip.elastic_ip}"
}

output "cluster-name" {
  value = "${module.eks.cluster-name}"
}


output "auth_token" {
  value = "${module.redis_cluster.auth_token}"
}
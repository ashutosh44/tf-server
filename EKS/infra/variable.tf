
variable "env" {
  description = "env to deploy into, should typically ab_dev/ab_qa/ab_prod"
  default = "ab_dev"
}

###### Variable for Region ##########
variable "infra_region" {
    description = "region for the infra"
    default = "us-west-2"
}
variable "key_credentials_path" {
    description = "key credentials path"
    default = "/home/opstree/.aws/credentials"
}

###################### VPC vars #####################
# VPC CIDR block
variable "vpc_cidr_block" {
    description = "The CIDR block for the VPC"
    default = "10.0.0.0/16"
}
# VPC Tenancy
variable "instanceTenancy" {
 default = "default"
}
# VPC dns support value
variable "dnsSupport" {
 default = true
}
# VPC dns host name value
variable "dnsHostNames" {
        default = true
}

###########Tag for VPC
variable "vpc_tag_name" {
    description = "Tag for VPC Name fields"
    default = "vpc"
}


################# INTERNET GATEWAY vars ##################
##### Integrnet Gateway Name
variable "igw_name" {
    description = "Name for Internet gateway"
    default = "igw"
}


###########variables for first public subnet########
#### CIDR Block
variable "cidr_pub_subnet_az_a" {
    description = "The CIDR block for the subnet."
    default = "10.0.15.0/24"
}
#### Tag Name 
variable "pub_subnet_name_az_a" {
    description = "name of public subnets"
    default = "pub_subnet_az_a"
}

###########variables for second public subnet########
#### CIDR Block
variable "cidr_pub_subnet_az_b" {
    description = "The CIDR block for the subnet."
    default = "10.0.16.0/24"
}
#### Tag Name 
variable "pub_subnet_name_az_b" {
    description = "name of public subnets"
    default = "pub_subnet_az_b"
}

######## Public subnet key value
variable "pub_subnet_key_value" {
  description = "Public subnet tag value for k8s"
  default = "kubernetes.io/role/elb"
}
###########variables for first Jenkins private subnet########
#### CIDR Block
variable "cidr_jenkins_pvt_subnet_az_a" {
    description = "The CIDR block for the subnet."
    default = "10.0.28.0/24"
}
#### Tag Name 
variable "jenkins_pvt_subnet_name_az_a" {
    description = "name of public subnets"
    default = "jenkins_pvt_subnet_az_a"
}

###########variables for second private subnet########
#### CIDR Block
variable "cidr_eks_pvt_subnet_az_a" {
    description = "The CIDR block for the subnet."
    default = "10.0.3.0/24"
}
#### Tag Name 
variable "eks_pvt_subnet_name_az_a" {
    description = "name of public subnets"
    default = "eks_cluster_pvt_subnet_az_a"
}
###########variables for second private subnet########
#### CIDR Block
variable "cidr_eks_pvt_subnet_az_b" {
    description = "The CIDR block for the subnet."
    default = "10.0.4.0/24"
}
#### Tag Name 
variable "eks_pvt_subnet_name_az_b" {
    description = "name of public subnets"
    default = "eks_cluster_pvt_subnet_az_b"
}
###########variables for DB first private subnet########
#### CIDR Block
variable "cidr_db_pvt_subnet_az_a" {
    description = "The CIDR block for the subnet."
    default = "10.0.10.0/24"
}
#### Tag Name
variable "db_pvt_subnet_name_az_a" {
    description = "name of public subnets"
    default = "db_pvt_subnet_az_a"
}
###########variables for DB second private subnet########
#### CIDR Block
variable "cidr_db_pvt_subnet_az_b" {
    description = "The CIDR block for the subnet."
    default = "10.0.11.0/24"
}
variable "db_pvt_subnet_name_az_b" {
    description = "name of public subnets"
    default = "db_pvt_subnet_az_b"
}

############ Priavte subnet key value ########33
variable "pvt_subnet_key_value" {
  description = "Private subnet tag value for k8s"
  default = "kubernetes.io/role/internal-elb"
}

####### Two Availability zone for all subnets#########3
variable "availability_zone_a" {
    description = "The AZ for the subnet"
    default = "us-west-2a"
} 
variable "availability_zone_b" {
    description = "The AZ for the subnet"
    default = "us-west-2b"
}

######### Variable for NAT Gateway
variable "natgateway_name" {
  default = "nat_gateway"
}
############# Variables for Public Route Table#############

### Public RT Name
variable "rt_pub_tag_name" {
  default = "pub_rt"
}
### CIDR Block
variable "cidr_block_pub_rt" {
  default = "0.0.0.0/0"
}
############# Variables for Private Route Table#############
###### Private RT Name
variable "rt_pvt_tag_name" {
  default = "pvt_rt"
}
### CIDR Block
variable "cidr_block_pvt_rt" {
  default = "0.0.0.0/0"
}


######## Variable for Jump server Security group ##############
### Jump server SG name
variable "jump_server_security_group_name" {
  default = "jump_server_sg"
}
###Jump server SG tag name
variable "jump_server_security_group_tag" {
  default = "jump_server_sg"
}
### Jump server SG CIDR 
variable "cidrs" {
  type = "list"
  default = ["61.95.220.249/32", "184.191.141.66/32", "171.50.132.18/32"]
}

#### SSH port value
variable "jump_server_ports" {
  default = "22"
}

######## Variable for EKS Workers Security group ##############
#####Port value
variable "from_ports" {
  default = "22,6443,1025"
}
variable "to_ports" {
  default = "22,6443,65535"
}
###EKS Workers SG name
variable "eks_security_group_name" {
  default = "eks_sg"
}
### EKS Worker sg tag name
variable "eks_security_group_tag" {
  default = "eks_sg"
}
######## Variable for  alb Security group ##############
#####Port value### Jump server SG CIDR 
variable "jenkins_cidrs" {
  type = "list"
  default = ["61.95.220.249/32", "184.191.141.66/32", "171.50.132.18/32"]
}

#### SSH port value
variable "jenkins_server_ports" {
  default = "80"
}
###alb Workers SG name
variable "alb_security_group_name" {
  default = "alb_sg"
}
### alb Worker sg tag name
variable "alb_security_group_tag" {
  default = "alb_sg"
}

######## Variable for  jenkins Security group ##############
#####Port value
variable "from_ports_jenkins" {
  default = "8080,80"
}
variable "to_ports_jenkins" {
  default = "8080,80"
}
###alb Workers SG name
variable "jenkins_security_group_name" {
  default = "jenkins_sg"
}
### alb Worker sg tag name
variable "jenkins_security_group_tag" {
  default = "jenkins_sg"
}
######## variable for key name #########
variable "key" {
  description = "Enter Key name"
  default = "ab_dev_key"
}


######## Variable for  redis Security group ##############
#####Port value
variable "from_ports_redis" {
  default = "6379"
}
variable "to_ports_redis" {
  default = "6379"
}
###EKS Workers SG name
variable "redis_security_group_name" {
  default = "redis_sg"
}
### EKS Worker sg tag name
variable "redis_security_group_tag" {
  default = "redis_sg"
}


######## Variable for Jump server#########
variable "instance_ami" { 
default = "ami-0869af7c7f93393ac"
}
###  Instance type
variable "instance_type" {
  default = "t2.medium"
}
### instance volume type
variable "volume_type" {
  description = "instance volume type"
  default = "gp2"
}
### Instance Volume size
variable "volume_size" {
  description = "instance volume size"
  default = "50"
}

### instance termination value
variable "delete_on_termination" {
  description = "instance delete on termination"
  default = "true"
}

### Server name
variable "server_name" {
  description = "jump Server Name"
  default = "jump_server"
}

######## Variable for Jenkins server#########
variable "jenkins_ami" { 
default = "ami-074493ce6a3632cfb"
}
###  Instance type
variable "jenkins_instance_type" {
  default = "t2.micro"
}
### instance volume type
variable "jenkins_volume_type" {
  description = "instance volume type"
  default = "gp2"
}
### Instance Volume size
variable "jenkins_volume_size" {
  description = "instance volume size"
  default = "50"
}


### Server name
variable "jenkins_server_name" {
  description = "jump Server Name"
  default = "jenkins_server"
}

######### Elastic Ip variable #######
variable "elastic_ip_tag_value" {
  default = "jump_server_eip"
  description = "elastic ip name"
  
}
########### Variable for eks cluster##########

variable "k8s_version" {
  description = "k8s version"
  default = "1.13"
 
}

variable "cluster_name" {
  description = "Cluster Name"
  default = "eks_cluster"
}

########## Variable for EKS worker launch configuration######
###### Instance Type
variable "instance_type_worker" {
  default = "t3a.large"
}

########## variable of eks worker autoscaling group ######
variable "desired_capacity" {
  description = "The number of EC2 instances that should be running in the group"
  default = "3"
}

variable "max_size" {
  description = "The maximum size of the auto scale group"
  default = "5"
}

variable "min_size" {
  description = "The minimum size of the auto scale group"
  default = "3"
}

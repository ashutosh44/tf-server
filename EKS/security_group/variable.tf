variable "to_ports" {
  default = "default_null"
}
variable "from_ports" {
  default = "default_null"
}

variable "source_security_group_id" {
  default = []
}

variable "security_group_name" {}

variable "vpc_id" {}

variable "tag_name_value" {
    description = "The value of tag Name"
}
